<?php include('header.php'); ?>  
  <!-- Wrapper Open -->
  <div class="wrapper">
    <!-- ep-header Open -->
    <div class="ep-header">
      <!-- Section 1 Open -->
      <div id="slide-home" class="section section1">
        <div class="ep-slider slide-fade">
          <!-- Item Open -->
          <div class="item" style="background-image: url('img/bg-item-borgor.jpg');">
            <div class="container">
              <div class="row">
                <div class="col-md-9 col-sm-9 col-center">
                  <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12 hidden-xs">
                      <div class="featured-image">
                        <img alt="...." class="img-responsive" src="img/borgor.png" width="320" height="427" />
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                      <h2>
                        <img alt="Borgor" class="artist-logo img-responsive" src="img/logo-borgor2.png"   />
                      </h2>
                      <div class="entry">
                        <strong class="semi-bold">Super 24 Puerto San José</strong>
                        <div class="content-event">
                          <div class="info-event">
                          <strong>28 Dec / 8:00 PM</strong>
                          <a class="link" href="step1.php">Comprar Tickets ></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Item Close -->    
          <!-- Item Open -->
          <div class="item" style="background-image: url('img/bg-w&w.jpg');">
            <div class="container">
              <div class="row">
                <div class="col-md-9 col-sm-9 col-center">
                  <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-12 hidden-xs">
                      <div class="featured-image">
                        <img alt="...." class="img-responsive" src="img/w&w.png" width="320" height="427" />
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-7 col-xs-12">
                      <h2>
                        <img alt="Borgor" class="artist-logo img-responsive" src="img/w&w-logo2.png" />
                      </h2>
                      <div class="entry">
                        <strong class="semi-bold">Super 24 Puerto San José</strong>
                        <div class="content-event">
                          <div class="info-event">
                          <strong>28 Dec / 8:00 PM</strong>
                          <a class="link" href="step1-1.php">Comprar Tickets ></a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Item Close -->                
        </div>
      </div>
      <!-- Section 1 Close -->
    </div>
    <!-- ep-header Close -->
    <!-- ep-body Open -->
    <div id="ep-body">
      <!-- Section 2 Open -->
      <div class="section section2">
        <div class="container">
          <div class="row">
            <!-- ep-content Open -->
            <div id="ep-content" class="col-md-8 col-sm-7 col-xs-12">
              <div id="ep-events">
                <div class="row">
                  <div class="col-md-12">
                    <h2 class="title-section bold"><span>Eventos</span></h2>
                  </div>
                </div>
                <div class="row">
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="R3HAB" href="single-event.php" class="bg-plus transition-ease"></a>                      
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0000_R3hab.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="R3HAB" href="single-event.php">R3HAB</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>
                  </div>
                  <!-- item Close -->                           
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Oliver Heldens" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0001_Heldens1.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="R3HAB" href="single-event.php">Oliver Heldens</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Halloween Heroes" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0002_HalloweenHeroes-3.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Halloween Heroes" href="single-event.php">Halloween Heroes</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Green Velvet" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0003_green-velvet.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Green Velvet" href="single-event.php">Green Velvet</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Celebrate Your Birthday" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0004_flyer-bday.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Celebrate Your Birthday" href="single-event.php">Celebrate Your Birthday</a></h3>
                      <strong class="location lighter">Club ONE</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Evashaw" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0005_evashaw.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Evashaw" href="single-event.php">Evashaw</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                                                                                                                
                </div>
              </div>
            </div>
            <!-- ep-content Close -->
            <!-- ep-sidebar Open -->
            <div id="ep-sidebar" class="col-md-4 col-sm-5 col-xs-12">
              <?php include('sidebar.php') ?>
            </div>
            <!-- ep-sidebar Close -->            
          </div>
        </div>
      </div>
      <!-- Section 2 Close -->
    </div>
    <!-- ep-body Open -->
    <?php include('sub-footer.php'); ?>
  </div>
  <!--Wrapper Close -->
<?php include('footer.php'); ?>