<?php include('header.php'); ?>  
  <!-- Wrapper Open -->
  <div id="page-step1" class="wrapper page-content">
    <!-- ep-header Open -->
    <div class="ep-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 col-xs-12">
            <div class="featured-Image">
              <h1 class="post-title">
                <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
              </h1>
            </div>
            <div class="location align-center">
              <strong class="semi-bold">Super 24  Puerto San Jos&eacute;</strong>
            </div>
          </div>
          <div class="col-md-6 col-sm-7 col-xs-12">
            <div class="entry">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-header Close -->
    <!-- ep-body Open -->
    <div id="ep-body">
      <div class="container section section1">
        <div class="row">
          <div class="col-md-10 col-center bg-single">
            <div class="row" id="addItems">
              <div class="col-xs-12">
                <h2 class="title-section bold"><span>Selecciona y añade</span></h2>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="input-group input-group-lg">
                  <span id="sizing-addon1" class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
                   <input type="text" name="show" class="form-control" id="show" readonly="readonly" disabled="disabled" value="Funcion 1" />
                   <!-- <select name="show" class="form-control" id="show">
                      <option value="funcion 1">Función 1</option>
                      <option value="funcion 2">Función 2</option>
                      <option value="funcion 3">Función 3</option>
                      <option value="funcion 4">Función 4</option>
                      <option value="funcion 5">Función 5</option>
                      <option value="funcion 6">Función 6</option>
                    </select> -->
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                <div class="input-group input-group-lg">
                  <span id="sizing-addon1" class="input-group-addon"><span class="glyphicon glyphicon-map-marker"></span></span>
                    <select name="locality" class="form-control" id="locality">
                      <option value="Localidad 1">Localidad 1</option>
                      <option value="Localidad 2">Localidad 2</option>
                      <option value="Localidad 3">Localidad 3</option>
                      <option value="Localidad 4">Localidad 4</option>
                      <option value="Localidad 5">Localidad 5</option>
                      <option value="Localidad 6">Localidad 6</option>
                    </select>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                <div class="input-group input-group-lg">
                  <span id="sizing-addon1" class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>          
                    <select name="people" class="form-control" id="people">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>                                                                                   
                    </select>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 align-right">
                <button class="btn btn-custom btn-black transition-ease" id="add-item-action"><span class="glyphicon glyphicon-plus"></span> añadir</button>
              </div>        
            </div>
            <div class="row">
              <div class="col-md-12">
                <h2 class="title-section bold"><span>Detalle de la factura</span></h2>
              </div>
            </div>
            <div id="details" class="row">
              <div class="col-md-12">
                <div class="t-scroll">
                  <table class="detailsItems table table-hover">
                    <thead>
                      <tr>
                        <td>Función</td>
                        <td>Localidad</td>
                        <td class="align-center">Cant.</td>
                        <td class="align-right">P.U.</td>
                        <td class="align-right">Total</td>
                        <td class="align-cener">Acción</td>                
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                      <tr class="total-detail">
                        <td class="align-right" colspan="4">
                          <strong class="total-f">Total:</strong>
                        </td>
                        <td class="align-right">
                          <span id="totalFact">0.00</span>
                        </td>
                        <td>&nbsp;</td>
                      </tr>
                    </tfoot>
                  </table>
                </div>                  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 align-center">
                <a id="next-step" class="btn btn-custom btn-yellow transition-ease disabled" href="step2.php" title="Comprar Tickets">Siguiente</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-body Close -->    
    <?php include('sub-footer.php'); ?>        
  </div>
  <!-- Wrapper Open -->
    <script type="text/javascript">
      jQuery(document).ready(function($){
        
        var argsPrice = [];
        var $next_step =  $('#next-step');
        
        function add_item(){
          var show = $('#show').val();
          var locality = $('#locality').val();
          var people = $('#people').val();
          var price = 150.00;
          var template = template_add_item(show, locality, people, price);
          push_total(price, people);
          return template;
        }
        
        function push_total(price, people){
          argsPrice.push((price*people));
        }
                
        function template_add_item(show, locality, people, price){
          var template = '';
          template += '<tr>';
          template += '<td class="d-item show-item">'+show+'</td>';
          template += '<td class="d-item locality-item">'+locality+'</td>';          
          template += '<td class="d-item people-item align-center">'+people+'</td>';          
          template += '<td class="d-item price-item align-right">'+price+'</td>';  
          template += '<td class="d-item total-item align-right">'+(price * people)+'</td>';
          template += '<td class="d-item action-item align-center"><button class="btnRemove btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button></td>'; 
          template += '</tr>';          
          return template;
        }
        
        function next_step_active(){
          (!$next_step.hasClass('disabled') && $('tbody tr').length < 1 ) ? $next_step.addClass('disabled') : $next_step.removeClass('disabled');
        }
        
        $('#add-item-action').click(function(){
          var html = add_item();
          $('#details  .detailsItems tbody').append(html);
          next_step_active();
        });
        
        $('#details table').on('click', '.btnRemove', function(){
          $(this).parent().parent().remove();
          next_step_active();          
        });
        
      });
  </script>  
<?php include('footer.php'); ?>      