<?php include('header.php'); ?>  
  <!-- Wrapper Open -->
  <div id="events" class="wrapper single-page">
    <!-- ep-header Open -->
    <div class="ep-header">
      <style>
        .ep-header{background-image: url('img/bg.jpg');}
      </style>
      <div class="container align-center">
        <h1 class="sub-title semi-bold">Lorem Ipsum dolor sit amet</h1>
      </div>
    </div>
    <!-- ep-header Open --> 
    <!-- ep-body Open -->
    <div id="ep-body">
      <!-- Section 2 Open -->
      <div class="section section2">
        <div class="container">
          <div class="row">
            <!-- ep-content Open -->
            <div id="ep-content" class="col-md-8 col-sm-7 col-xs-12">
              <div id="ep-events">
                <div class="row">
                  <div class="col-md-12">
                    <h2 class="title-section bold"><span>Eventos</span></h2>
                  </div>
                </div>
                <div class="row">
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="R3HAB" href="single-event.php" class="bg-plus transition-ease"></a>                      
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0000_R3hab.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="R3HAB" href="single-event.php">R3HAB</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>
                  </div>
                  <!-- item Close -->                           
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Oliver Heldens" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0001_Heldens1.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="R3HAB" href="single-event.php">Oliver Heldens</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Halloween Heroes" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0002_HalloweenHeroes-3.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Halloween Heroes" href="single-event.php">Halloween Heroes</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Green Velvet" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0003_green-velvet.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Green Velvet" href="single-event.php">Green Velvet</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Celebrate Your Birthday" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0004_flyer-bday.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Celebrate Your Birthday" href="single-event.php">Celebrate Your Birthday</a></h3>
                      <strong class="location lighter">Club ONE</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Evashaw" href="single-event.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0005_evashaw.jpg" />
                    </div>
                    <div class="entry transition-ease align-center">
                      <h3 class="post-title family-bebas"><a title="Evashaw" href="single-event.php">Evashaw</a></h3>
                      <strong class="location lighter">Puerto de San Jos&eacute;</strong>
                    </div>                    
                  </div>
                  <!-- item Close -->                                                                                                                
                </div>
              </div>
              
              <div id="ep-events" class="event-past">
                <div class="row">
                  <div class="col-md-12">
                    <h2 class="title-section bold"><span>Eventos Pasdos</span></h2>
                  </div>
                </div>
                <div class="row">
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="R3HAB" href="single-event2.php" class="bg-plus transition-ease"></a>                      
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0000_R3hab.jpg" />
                    </div>
                  </div>
                  <!-- item Close -->                           
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Oliver Heldens" href="single-event2.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0001_Heldens1.jpg" />
                    </div>                   
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Halloween Heroes" href="single-event2.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0002_HalloweenHeroes-3.jpg" />
                    </div>                   
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Green Velvet" href="single-event2.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0003_green-velvet.jpg" />
                    </div>                  
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Celebrate Your Birthday" href="single-event2.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0004_flyer-bday.jpg" />
                    </div>               
                  </div>
                  <!-- item Close -->                
                  <!-- item Open -->
                  <div id="event-slug" class="col-md-4 col-sm-6 col-xs-6 item">
                    <div class="featured-image imgContentGallery">
                      <a title="Evashaw" href="single-event2.php" class="bg-plus transition-ease"></a>                           
                      <img class="img-responsive transition-ease" alt="...." src="img/flayers/1up_0005_evashaw.jpg" />
                    </div>                 
                  </div>
                  <!-- item Close -->                                                                                                                
                </div>
              </div>              
              
            </div>
            <!-- ep-content Close -->
            
            <!-- ep-sidebar Open -->
            <div id="ep-sidebar" class="col-md-4 col-sm-5 col-xs-12">
              <?php include('sidebar.php') ?>
            </div>
            <!-- ep-sidebar Close -->            
          </div>
        </div>
      </div>
      <!-- Section 2 Close -->
    </div>
    <!-- ep-body Open -->
    <?php include('sub-footer.php'); ?>
  </div>
  <!--Wrapper Close -->
<?php include('footer.php'); ?>