<?php include('header.php'); ?>  
  <!-- Wrapper Open -->
  <div id="page-step1" class="wrapper page-content">
    <!-- ep-header Open -->
    <div class="ep-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 col-xs-12">
            <div class="featured-Image">
              <h1 class="post-title">
                <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
              </h1>
            </div>
            <div class="location align-center">
              <strong class="semi-bold">Super 24  Puerto San Jos&eacute;</strong>
            </div>
          </div>
          <div class="col-md-6 col-sm-7 col-xs-12">
            <div class="entry">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-header Close -->
    <!-- ep-body Open -->
    <div id="ep-body">
      <div class="container section section1">
        <div class="row">
          <div class="col-md-10 col-center bg-single">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <h2 class="title-section bold"><span>Localidades</span></h2>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-locality align-right">
                  <ul>
                    <li style="background-color: #e8b733">
                      <span class="name-lality">
                        VIP
                      </span>
                      <span class="price-locality">Q 1000</span>
                    </li>
                    <li style="background-color: #bdcb7b">
                      <span class="name-lality">
                        Dance Floor
                      </span>
                      <span class="price-locality">Q 350</span>                      
                    </li>                    
                  </ul>
                </div>
              </div>
            </div>
            <div class="row" id="addItems">
              <div class="col-xs-12 align-center">
                <strong class="semi-bold labelevent2"><span>Selecciona la cantidad de entrada</span></strong>
              </div>
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 col-center">
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="input-group input-group-lg">
                      <span id="sizing-addon1" class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
                       <input type="text" name="show" class="form-control" id="show" readonly="readonly" disabled="disabled" value="Funcion 1" />
                       <!-- <select name="show" class="form-control" id="show">
                          <option value="funcion 1">Función 1</option>
                          <option value="funcion 2">Función 2</option>
                          <option value="funcion 3">Función 3</option>
                          <option value="funcion 4">Función 4</option>
                          <option value="funcion 5">Función 5</option>
                          <option value="funcion 6">Función 6</option>
                        </select> -->
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="input-group input-group-lg">
                      <span id="sizing-addon1" class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>          
                        <select name="people" class="form-control" id="people">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>                                                                                   
                        </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>            
            <div id="details" class="row image-map">              
              <div class="col-md-12 locality-image-content align-center">
                <strong class="semi-bold labelevent2 align-center"><span>Selecciona Localidad</span></strong>                
                <img class="map-locality" src="img/localidades.jpg" alt="" usemap="#Map" />
                <map name="Map" id="Map">
                    <area alt="" title="" href="step1-2.php" shape="poly" coords="13,180,138,181,139,525,10,525" />
                    <area alt="" title="" href="step1-2.php" shape="poly" coords="165,180,639,179,642,525,165,526" />
                    <area alt="" title="" href="step1-2.php" shape="poly" coords="665,180,793,179,794,525,666,525" />
                </map>                  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="entry">
                  <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 align-center">
                <a id="next-step" class="btn btn-custom btn-yellow transition-ease disabled" href="step2.php" title="Comprar Tickets">Siguiente</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-body Close -->    
    <?php include('sub-footer.php'); ?>        
  </div>
  <!-- Wrapper Open -->
<?php include('footer.php'); ?>      