<?php include('header.php') ?>
  <!-- Wrapper Open -->
  <div id="single-event" class="wrapper page-content">
    <!-- ep-header Open -->
    <div class="ep-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 col-xs-12">
            <div class="featured-Image">
              <h1 class="post-title">
                <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
              </h1>
            </div>
            <div class="location align-center">
              <strong class="semi-bold">Super 24  Puerto San Jos&eacute;</strong>
            </div>
          </div>
          <div class="col-md-6 col-sm-7 col-xs-12">
            <div class="entry">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-header Close -->  
    <!-- ep-body Open -->  
    <div id="ep-body">
      <div class="container content-single">
        <div class="row">
          <!-- bg-single Open -->
          <div class="col-md-10 col-center bg-single">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                <div class="content-event">
                  <div class="info-event">
                  <strong>Tickets desde Q.350</strong>
                  <a href="step1.php" class="link">Comprar Tickets &gt;</a>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <h2 class="title-section bold first-title"><span>Funciones</span></h2>
                <div class="list-function">
                  <ul>
                    <li>
                      <ul>
                        <li class="date-event">
                          <span class="glyphicon glyphicon-calendar custom-icon"></span>
                          <strong class="bold">28 de diciembre</strong>
                          <span class="time lighter">5:00 pm</span>
                        </li>
                        <li class="location-event">
                          <span class="glyphicon glyphicon-map-marker custom-icon"></span>
                          <strong class="bold">Super 24</strong>
                          <span class="time lighter">Puerto de San Jos&eacute;</span>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <li class="date-event">
                          <span class="glyphicon glyphicon-calendar custom-icon"></span>
                          <strong class="bold">1 de enero</strong>
                          <span class="time lighter">5:00 pm</span>
                        </li>
                        <li class="location-event">
                          <span class="glyphicon glyphicon-map-marker custom-icon"></span>
                          <strong class="bold">Super 24</strong>
                          <span class="time lighter">Puerto de San Jos&eacute;</span>
                        </li>
                      </ul>
                    </li>                    
                  </ul>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="bg-single2 col-md-12">
                <div class="row gallery-artist">
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_1.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_1.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div>          
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_2.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_2.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div>    
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_3.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_3.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div> 
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-7 col-xs-12">
                 <h2 class="title-section bold"><span>Video</span></h2>                
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe width="560" height="315" frameborder="0" allowfullscreen="" src="https://www.youtube.com/embed/beJ_yIL1_kI" class="embed-responsive-ite"></iframe>
                </div>
              </div>
              <div class="col-md-6 col-sm-5 col-xs-12">
                <?php include('wdgts/social-media.php') ?>  
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 align-center">
                <a class="btn btn-custom btn-yellow transition-ease" href="step1.php" title="Comprar Tickets">Comprar</a>
              </div>
            </div>
          </div>
          <!-- bg-single Close -->
        </div>
      </div>
    </div>
    <!-- ep-body Close -->
    <?php include('sub-footer.php'); ?>    
  </div>
  <!-- Wrapper Close -->  
<?php include('footer.php') ?>