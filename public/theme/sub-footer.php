<!-- sub-footer Open -->
<div id="ep-subfooter">
  <!-- Ep-links Open -->
  <div id="ep-links" class="section section-subfooter1">
    <div class="container">
      <div class="row">
        <!-- item Open -->
        <div class="col-md-4 col-sm-4 col-xs-12 align-center">
          <a title="Promociones" href="#">
            <img alt="" class="img-responsive" src="img/promociones.jpg" />
          </a>  
          <div class="entry">
            <h2 class="post-title"><a title="Promociones" href="#">Promociones</a></h2>
          </div>
        </div>
        <!-- item Close -->
        <!-- item Open -->
        <div class="col-md-4 col-sm-4 col-xs-12 align-center">
          <a title="Promociones" href="#">
            <img alt="" class="img-responsive" src="img/tickets.jpg" />
          </a>  
          <div class="entry">
            <h2 class="post-title"><a title="Promociones" href="#">Cortesias</a></h2>
          </div>
        </div>
        <!-- item Close -->
        <!-- item Open -->
        <div class="col-md-4 col-sm-4 col-xs-12 align-center">
          <a title="Promociones" href="#">
            <img alt="" class="img-responsive" src="img/Acreditaciones.jpg" />
          </a>  
          <div class="entry">
            <h2 class="post-title"><a title="Promociones" href="#">Acreditaciones</a></h2>
          </div>
        </div>
        <!-- item Close -->                
      </div>
    </div>
  </div>
  <!-- Ep-links Close -->  
  <!-- ep-suscribe Open -->
  <div id="ep-suscribe" class="section section-subfooter2">
    <div class="container">
      <div class="row">
        <div class="col-md-7 col-sm-8 col-xs-12 col-center">
          <h2 class="post-title semi-bold align-center">Suscripci&oacute;n</h2>
          <form>        
            <div class="input-group">
              <input class="form-control" type="email" placeholder="E-mail: lalvarez@ejemplo.com" />
              <span class="input-group-addon"><button class="btn btn-custom semi-bold">Enviar</button></span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- ep-suscribe Close -->  
  <!-- ep-contact Open -->
  <div id="ep-contact" class="section section-subfooter2">
    <div class="container">
      <h2 class="title-section bold"><span>Contacto</span></h2>
      <div class="row">
        <div class="col-md-9 col-sm-10 col-xs-12 col-center">
          <div class="entry">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex</p>
          </div>
          <div class="form-contant-content">
            <form>
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                  <div class="item">
                    <input type="text" name="name" placeholder="Nombre Completo" class="form-control" />
                  </div>
                  <div class="item">
                    <input type="email" name="email" placeholder="Correo Electr&oacute;nico" class="form-control" />
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="item">
                    <textarea class="form-control" name="message" placeholder="Mensaje"></textarea>
                  </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 align-right">
                  <button type="submit" class="btn btn-custom transition-ease">Enviar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ep-contact Close -->
  <!-- ep-info Open -->  
  <div id="ep-info" class="section section-subfooter2">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-sm-10 col-xs-12 col-center">
          <div class="row">
            <div class="col-md-6 col-sm-7 col-xs-12">
              <p class="address">1 av 12-34 zona 5. Edificio Lorem Ipsum, torre 2, nivel 10 oficina 1003, 01010-Ciudad de Guatemala</p>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 align-right">
              <span class="telephone-number">(502) 5555-5555</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ep-info Close -->    
</div>
<!-- sub-footer Close -->