  <!-- ep-footer Open -->
  <div id="ep-footer" class="copyright section">
    <div class="container">
      <div class="row">
      	<div class="col-md-4 col-sm-4 col-xs-12 align-right pull-right">
      		<ul class="social">
      		  <li>
      				<a href="#" title="Facebook" target="_blank"><span class="iconSocial icon-facebook"></span></a>
      			</li>	    					    					
      			<li>
      				<a href="#" title="Twitter" target="_blank"><span class="iconSocial icon-twitter"></span></a>      					
      			</li>    		
      			<li>
      				<a href="#" title="Instagram" target="_blank"><span class="iconSocial icon-instagram"></span></a>      					
      			</li>    	
      			<li>
      				<a href="#" title="YouTube" target="_blank"><span class="iconSocial icon-youtube"></span></a>      					
      			</li>    		      				      						
      		</ul>
      	</div>
        <div class="col-md-4 col-sm-4 col-xs-12">
      		<p>Todos los derechos reservados Empire Promotion 2015</p> 
      	</div>					
      	<div class="col-md-4 col-sm-4 col-xs-12 align-center pull-right">
      		<a href="http://www.webcenter.com.gt" target="_blank" rel="bookmark" title="WebCenter">
      			<img src="assets/img/logo-webcenter.png" alt="WebCenter">
      		</a>
      	</div>
      </div>
    </div>
  </div>
  <!-- ep-footer Close -->  
  <!-- Scripts --> 
  <script type="text/javascript" src="assets/js/bootstrap.js?ver=3.3.4"></script>
  <script type="text/javascript" src="assets/js/slick.js?ver=1.5"></script>
  <script type="text/javascript" src="assets/js/fancybox.js?ver=2.1.5"></script>    
  <script type="text/javascript" src="assets/js/app.js?ver=1.1"></script>
  <!-- Scripts Single Event -->  
  <script type="text/javascript" src="assets/js/plugins.js?v=1.6"></script>
  <script type="text/javascript" src="assets/js/site.js?v=1.6"></script>
  <script type="text/javascript" src="assets/js/social.stream.js?v=1.5.1"></script>
 </body>
</html>