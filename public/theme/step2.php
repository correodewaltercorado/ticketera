<?php include('header.php') ?>
  <!-- Wrapper Open -->
  <div id="single-event" class="wrapper page-content">
    <!-- ep-header Open -->
    <div class="ep-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 col-xs-12">
            <div class="featured-Image">
              <h1 class="post-title">
                <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
              </h1>
            </div>
            <div class="location align-center">
              <strong class="semi-bold">Super 24  Puerto San Jos&eacute;</strong>
            </div>
          </div>
          <div class="col-md-6 col-sm-7 col-xs-12">
            <div class="entry">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-header Close -->
    <!-- ep-body Open -->
    <div id="ep-body">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-center bg-single">
            
            <div class="row">
              <div class="col-xs-12">
                <div class="bgContent">
                <div class="row">
                  <div class="col-md-12">
                    <h2 class="title-section bold"><span>Datos del cliente</span></h2>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 co-sm-6 col-xs-6 item">
                    <label for="name">Nombre:</label>
                    <input type="text" required="required" value="" placeholder="Jorge Mario" name="name" id="name" class="form-control">
                  </div>
                  <div class="col-md-6 co-sm-6 col-xs-6 item">
                    <label for="last-name">Apellidos:</label>
                    <input type="text" required="required" value="" placeholder="Alvarez Corado" name="last-name" id="last-name" class="form-control">
                  </div>            
                </div>
                <div class="row">
                  <div class="col-md-6 co-sm-6 col-xs-6 item">
                    <label for="email">Correo Electrónico:</label>
                    <input type="email" required="required" value="" placeholder="jalvarez@ejemplo.com" name="email" id="email" class="form-control">
                  </div>
                  <div class="col-md-6 co-sm-6 col-xs-6 item">
                    <label for="phone">Teléfono:</label>
                    <input type="text" required="required" value="" placeholder="5555-5555" name="phone" id="phone" class="form-control">
                  </div>            
                </div>
                <div class="row">
                  <div class="col-md-6 co-sm-6 col-xs-6 item">
                    <label for="address">Dirección:</label>
                    <input type="text" required="required" value="" placeholder="3a. calle 8-25, zona 12, Guatemala, Guatemala" name="address" id="address" class="form-control">
                  </div>
                  <div class="col-md-3 co-sm-3 col-xs-3 item">
                    <label for="city">Ciudad:</label>
                    <input type="text" required="required" value="" placeholder="Guatemala" name="city" id="city" class="form-control">
                  </div>    
                  <div class="col-md-3 co-sm-3 col-xs-3 item">
                    <label for="code">Código Postal:</label>
                    <input type="text" required="required" value="" placeholder="010001" name="code" id="code" class="form-control">
                  </div>                        
                </div>          
              </div>
              </div>
            </div>  
            
            
            <div class="row">
              <div class="col-xs-12">
                <div class="row">
                  <div class="col-md-12">
                    <h2 class="title-section"><span>Datos de la tarjeta</span></h2>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 co-sm-12 col-xs-12 item">
                    <label for="number-card">Número de la tarjeta:</label>
                    <div class="row">
                      <div class="col-md-3 col-sm-3 col-xs-3">
                         <input type="text" required="required" value="" placeholder="xxxx" name="number-car1" id="number-car" class="form-control alignCenter">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3">
                         <input type="text" required="required" value="" placeholder="xxxx" name="number-car2" id="number-car2" class="form-control alignCenter">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3">
                         <input type="text" required="required" value="" placeholder="xxxx" name="number-car3" id="number-car3" class="form-control alignCenter">
                      </div>
                      <div class="col-md-3 col-sm-3 col-xs-3">
                         <input type="text" required="required" value="" placeholder="xxxx" name="number-car4" id="number-car4" class="form-control alignCenter">
                      </div>                                                
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <label for="month">Mes</label>
                    <select name="month" id="month" class="form-control">
                        <option selected="" disabled="">Selecciona un mes</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>                                                                                                                                                                                                      
                    </select>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <label for="month">Año</label>
                    <select name="month" id="month" class="form-control">
                        <option selected="" disabled="">Selecciona un año</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                        <option value="2026">2026</option>                                                                                                                                                                                                      
                    </select>
                  </div>
                  
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <label for="month">Código CVV</label>
                    <div class="input-group">
                      <input type="text" class="form-control validate[required]" autocomplete="off" id="cvv" placeholder="xxx" name="cvv" maxlength="3">
                       <a data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-placement="top" data-toggle="popover" data-container="body" data-trigger="focus" href="#" class="input-group-addon" id="viewCvv">Ver CVV</a> 
                      <div class="contentImageCVV" style="display: none;">
                        <img class="img-responsive" src="assets/img/cvv.png">
                      </div>                  
                      <script type="text/javascript">
                        jQuery(document).ready(function($){
                          var $viewCvv = $('#viewCvv');
                          var imageCVV = $('.contentImageCVV').html();
                          $viewCvv.click(function(e){
                            e.preventDefault();
                          });
                          $viewCvv.popover({
                            html : true,
                            template : '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div><div class="cvvImageContet">'+imageCVV+'</div></div>'
                          });
                        });
                      </script>                 
                    </div>
                  </div>
                  
                </div>
                <div class="row marginTop30">
                  <div class="col-xs-12 align-right">
                    <a id="pay" class="btn btnCustom btn-black" href="pay.html">Aceptar</a>
                  </div>            
                </div>
                
              </div>
            </div>  
            
            
            <div class="row">
              <div class="col-md-12">
                <h2 class="title-section bold"><span>Detalle de la compra</span></h2>
              </div>
              <div class="col-md-12">
                <div class="t-scroll">
                  <table class="detailsItems table table-hover">
                    <thead>
                      <tr>
                        <td>Función</td>
                        <td>Localidad</td>
                        <td class="align-center">Cant.</td>
                        <td class="align-right">P.U.</td>
                        <td class="align-right">Total</td>                
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="d-item show-item">Funcion 1</td>
                        <td class="d-item locality-item">Localidad 3</td>
                        <td class="d-item people-item align-center">4</td>
                        <td class="d-item price-item align-right">150</td>
                        <td class="d-item total-item align-right">600</td>
                      </tr>
                      <tr>
                        <td class="d-item show-item">Funcion 1</td>
                        <td class="d-item locality-item">Localidad 3</td>
                        <td class="d-item people-item align-center">4</td>
                        <td class="d-item price-item align-right">150</td>
                        <td class="d-item total-item align-right">600</td>
                      </tr>                      
                    </tbody>
                    <tfoot>
                      <tr class="total-detail">
                        <td colspan="4" class="align-right">
                          <strong class="total-f">Total:</strong>
                        </td>
                        <td class="align-right">
                          <span id="totalFact">0.00</span>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>
    <!-- ep-body Close -->
    <?php include('sub-footer.php'); ?>    
  </div>
  <!-- Wrapper Open -->
<?php include('footer.php'); ?>  