<?php include('header.php'); ?>  
  <!-- Wrapper Open -->
  <div id="page-step1" class="wrapper page-content">
    <!-- ep-header Open -->
    <div class="ep-header">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-5 col-xs-12">
            <div class="featured-Image">
              <h1 class="post-title">
                <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
              </h1>
            </div>
            <div class="location align-center">
              <strong class="semi-bold">Super 24  Puerto San Jos&eacute;</strong>
            </div>
          </div>
          <div class="col-md-6 col-sm-7 col-xs-12">
            <div class="entry">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-header Close -->
    <!-- ep-body Open -->
    <div id="ep-body">
      <div class="container section section1">
        <div class="row">
          <div class="col-md-10 col-center bg-single">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <h2 class="title-section bold"><span>Localidades</span></h2>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="info-locality align-right">
                  <ul>
                    <li style="background-color: #e8b733">
                      <span class="name-lality">
                        VIP
                      </span>
                      <span class="price-locality">Q 1000</span>
                    </li>
                    <li style="background-color: #bdcb7b">
                      <span class="name-lality">
                        Dance Floor
                      </span>
                      <span class="price-locality">Q 350</span>                      
                    </li>                    
                  </ul>
                </div>
              </div>
            </div>
     
            <div id="grid-locality" class="row">              
              <div class="col-md-12 locality-content align-center">
                <div class="space-content align-center locality-content">
                  
                  <div class="row">
                    <div class="col-xs-12">
                      <span class="item-number">&nbsp;</span>
                  <?php for($j=1;$j<=12;$j++){ ?>
                      <span class="item-number"><?php echo $j; ?></span>
                  <?php } ?>
                    </div>
                  </div>
                  
                  <?php $letter = 65; for($i=1;$i<=9;$i++){ ?>
                  <div class="row">
                    <div class="col-xs-12 align-center">
                        <span class="item-letter"><?php echo chr($letter); ?></span> <?php $letter= $letter +1; ?>
                        <?php $class = ($i == 3 ) ? ' reserved' : ''; ?>
                    <?php for($j=1;$j<=12;$j++){  ?>
                        <span class="item-locality<?php echo $class; ?>">&nbsp;</span>
                    <?php  } ?>
                    </div>
                  </div>
                  <?php } ?>      
                                
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-xs-12">
                <div class="entry">
                  <div class="locality-shop align-center">
                    <strong>Asientos:</strong> 
                    <span>E8</span>
                    <span>E9</span>
                    <span>E10</span>                                    
                  </div>
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
                  <p>"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>                
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12 align-center">
                <a id="next-step" class="btn btn-custom btn-yellow transition-ease disabled" href="step2.php" title="Comprar Tickets">Siguiente</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-body Close -->    
    <?php include('sub-footer.php'); ?>        
  </div>
  <!-- Wrapper Open -->
  <script>
    jQuery(document).ready(function(){
      var $item = $('.item-number');
      var item_count = $item.length;
      var item_size = $item.width()+30;
      console.log(item_size);
      $('.space-content').width(item_size*item_count);
      
      $('.item-locality').click(function(){
        if (!$(this).hasClass('selected') && !$(this).hasClass('reserved')){
          $(this).addClass('selected');
        }else{
          $(this).removeClass('selected');
        }
      });
      
    });
  </script>  
    
<?php include('footer.php'); ?>      