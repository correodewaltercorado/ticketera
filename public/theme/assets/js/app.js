/*
Theme Name: SmartTicket
Theme URI: http://smart.com/
Author: WC
Author URI: http://wC.com/
Version: 1.1
Creation: 02-10-2018
Last modified: 07-05-2015
Description: System Ticket.
Tags: System Ticket, SmartTicket, Bootstrap, jQuery, Slick, Fancybox, Slideshow, Gallery, Calendar Events, Contact, Suscribe, Social Media
*/
jQuery(document).ready(function($){
  /*Logo animated*/
  $('#site-title img').mouseenter(function(){
    $(this).addClass('swing');
  }).mouseleave(function(){
    $(this).removeClass('swing');
  });
  //Slider fade
  $('.ep-slider.slide-fade').slick({
    dots: true,
    arrows: false,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 6000,
    fade: true,
    cssEase: 'linear'
  });
  $(".light-box").fancybox({
		openEffect	: 'fade',
		closeEffect	: 'fade',
    prevEffect		: 'fade',
		nextEffect		: 'fade',	
	});
	$("a[href*='#ep-contact']").click(function(event) {
    //if ($('body').hasClass('home')){
      event.preventDefault();
      var full_url = this.href;
      var parts = full_url.split("#");
      var trgt = parts[1];
      var target_offset = $("#" + trgt).offset();
      var target_top = target_offset.top - 50;
      $('html, body').animate({
          scrollTop: target_top
      }, 2500);
    //}
  });
});

function loading(){

  jQuery('#loading-div').css({
    position:'fixed',
    left: (jQuery(window).width() - jQuery('#loading-div').outerWidth())/2,
    top: (jQuery(window).height() - jQuery('#loading-div').outerHeight())/2
  });
  jQuery('#loading-div').fadeIn();
}

function removeLoading(){
  jQuery('#loading-div').fadeOut();
}

function goToDiv(selector, offset, speed){
    var aTag = $(selector);
    offset = typeof offset !== 'undefined' ? offset : 0;
    speed  = typeof speed  !== 'undefined' ? speed : 'fast';
    $('html,body').animate({scrollTop: aTag.offset().top + offset}, speed);
}

function message(text){
  humane.remove();
  humane.timeout = 5000;
  humane.log(text);
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};