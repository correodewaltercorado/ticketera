/* Script JS */
$(document).ready(function() { 
    // Function animate width Search
    $('#searchButton').click(function(e) {
        var width = $('#searchInput').width();
        if (width == 0) {
            $('#searchInput').animate({'width' : '200px', 'padding' : '5px 10px'}, 400, function() {
                $(this).attr('value', '').focus();
                $(this).addClass('inputOpen');
            }); 
        }
        e.preventDefault();
    });
    $('#searchInput').blur(function() {
        $(this).animate({'width': '0px', 'padding' : '5px 0' }, 400, function(){
            $('#searchInput').attr('value', 'Buscar...');
        });
    });
});