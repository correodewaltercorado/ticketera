<!-- Social Media Open -->
<div class="social-widget wdgt">
  <h2 class="title-section bold"><span>Redes Sociales</span></h2>
  <div id="social" class="social-stream"></div>
  <script type="text/javascript">
    jQuery(document).ready(function($){
      $('#social').dcSocialStream({
  			feeds: {
  				twitter: {
  					id: '#emf2015',
  					//intro: 'Tweeted',
  					//search: 'Tweeted',
  					out: 'intro,thumb,text,share',
  					url: '/1up/lib/twitter.php',
  				},
  				/*facebook: {
  					id: '1726804334212217,Facebook Timeline/1402616913319867,#emf2015',
  					out: 'intro,thumb,text,user,share',
  					//image_width: 4,
  					url: '/1up/lib/facebook.php'
  				},*/
  				/*instagram: {
  					id: '#emf2014'
  				},*/
  			},	
  			/*rotate: {
  				delay: 0
  			},*/
  			//control: true,
  			//order: 'random',
  			//filter: false,
  			//wall: false,
  			cache: true,
  			//max: 'limit',
  			//limit: 20,
  			iconPath: '/1up/assets/img/dcsns-dark/',
  			imagePath: '/1up/assets/img/dcsns-dark/',
  			height: 275
  		});
    });
  </script>
</div>
<!-- Social Media Close -->