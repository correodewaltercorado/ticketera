<?php include('header.php') ?>
  <!-- Wrapper Open -->
  <div id="single-event" class="wrapper page-content single-page">
     <!-- ep-header Open -->
    <div class="ep-header">
      <style>
        .ep-header{background-image: url('img/bg.jpg');}
      </style>
      <div class="container align-center">
        <strong class="sub-title semi-bold">Lorem Ipsum dolor sit amet</strong>
      </div>
    </div>
    <!-- ep-header Open -->   
    <!-- ep-body Open -->  
    <div id="ep-body">
      <div class="container content-single">
        <div class="row">
          <!-- bg-single Open -->
          <div class="col-md-10 col-center bg-single">
            <div class="row">
              <div class="bg-single2 col-md-12">
                <div class="row gallery-artist">
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_1.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_1.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div>          
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_2.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_2.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div>    
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                  <!-- Gallery Item Open -->
                  <div class="col-md-4 col-sm-4 col-xs-4 item">
                    <div class="gallery-content">
                      <a title="Nombre de la galer&iacute;a" href="img/gallery/gallery_3.jpg" rel="gallery" class="light-box">
                        <img alt="..." class="img-responsive" src="img/gallery/320_3.jpg" />
                        <div class="bgGallery transition-ease align-center"><span class="glyphicon transition-ease glyphicon-zoom-in"></span></div> 
                      </a>
                    </div>
                  </div>
                  <!-- Gallery Item Close -->
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <div class="row">
                  <div class="col-md-12">
                    <div class="featured-Image">
                      <h1 class="post-title">
                        <img class="img-responsive" alt="Borgor" src="img/logo-borgor2.png" />
                      </h1>
                    </div>
                  </div>
                </div>            

                <div class="entry">
                  <h2>El evento m&aacute;s grande</h2>
                  <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"<p>
                  <?php include('wdgts/gallery.php'); ?>
                </div>
              </div>
              <div id="ep-sidebar" class="col-md-5 col-sm-5 col-xs-12">
                <?php //include('sidebar.php') ?>
                <?php include('wdgts/video.php'); ?>
                <?php include('wdgts/social-media.php') ?>                
              </div>
            </div>
          </div>
          <!-- bg-single Close -->
        </div>
      </div>
    </div>
    <!-- ep-body Close -->
    <?php include('sub-footer.php'); ?>    
  </div>
  <!-- Wrapper Close -->  
<?php include('footer.php') ?>