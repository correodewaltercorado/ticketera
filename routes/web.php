<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
})->middleware('auth');*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/quienes-somos', 'HomeController@quienessomos')->name('quienessomos');
Route::get('/eventos', 'HomeController@eventos')->name('eventos');
Route::get('/single', 'HomeController@single')->name('single');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


Route::get('/admin', 'AdminController@index')->name('admin')->middleware('auth');
Route::prefix('admin')->group(function () {
    
    Route::resource('settings', 'SettingsController')->middleware('auth');
    Route::resource('places', 'PlacesController')->middleware('auth');
    Route::resource('countries', 'CountriesController')->middleware('auth');
});


