<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ $setting->name }}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('nit') ? 'has-error' : ''}}">
    <label for="nit" class="control-label">{{ 'Nit' }}</label>
    <input class="form-control" name="nit" type="text" id="nit" value="{{ $setting->nit }}" >
    {!! $errors->first('nit', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('comercial_name') ? 'has-error' : ''}}">
    <label for="comercial_name" class="control-label">{{ 'Comercial Name' }}</label>
    <input class="form-control" name="comercial_name" type="text" id="comercial_name" value="{{ $setting->comercial_name }}" >
    {!! $errors->first('comercial_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('business_ name') ? 'has-error' : ''}}">
    <label for="business_ name" class="control-label">{{ 'Business  Name' }}</label>
    <input class="form-control" name="business_name" type="text" id="business_name" value="{{ $setting->business_name }}" >
    {!! $errors->first('business_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('mail') ? 'has-error' : ''}}">
    <label for="mail" class="control-label">{{ 'Mail' }}</label>
    <input class="form-control" name="mail" type="text" id="mail" value="{{ $setting->mail }}" >
    {!! $errors->first('mail', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('contact_text') ? 'has-error' : ''}}">
    <label for="contact_text" class="control-label">{{ 'Contact Text' }}</label>
    <input class="form-control" name="contact_text" type="text" id="contact_text" value="{{ $setting->contact_text }}" >
    {!! $errors->first('contact_text', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Address' }}</label>
    <input class="form-control" name="address" type="text" id="address" value="{{ $setting->address }}" >
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
    <label for="telephone" class="control-label">{{ 'Telephone' }}</label>
    <input class="form-control" name="telephone" type="text" id="telephone" value="{{ $setting->telephone }}" >
    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    <label for="facebook" class="control-label">{{ 'Facebook' }}</label>
    <textarea class="form-control" rows="5" name="facebook" type="textarea" id="facebook" >{{ $setting->facebook }}</textarea>
    {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('instagram') ? 'has-error' : ''}}">
    <label for="instagram" class="control-label">{{ 'Instagram' }}</label>
    <textarea class="form-control" rows="5" name="instagram" type="textarea" id="instagram" >{{ $setting->instagram }}</textarea>
    {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
    <label for="twitter" class="control-label">{{ 'Twitter' }}</label>
    <textarea class="form-control" rows="5" name="twitter" type="textarea" id="twitter" >{{ $setting->twitter }}</textarea>
    {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('youtube') ? 'has-error' : ''}}">
    <label for="youtube" class="control-label">{{ 'Youtube' }}</label>
    <textarea class="form-control" rows="5" name="youtube" type="textarea" id="youtube" >{{ $setting->youtube }}</textarea>
    {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('about_as') ? 'has-error' : ''}}">
    <label for="about_as" class="control-label">{{ 'About As' }}</label>
    <textarea class="form-control" rows="5" name="about_as" type="textarea" id="about_as" >{{ $setting->about_as }}</textarea>
    {!! $errors->first('about_as', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('mission') ? 'has-error' : ''}}">
    <label for="mission" class="control-label">{{ 'Mission' }}</label>
    <textarea class="form-control" rows="5" name="mission" type="textarea" id="mission" >{{ $setting->mission }}</textarea>
    {!! $errors->first('mission', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('vision') ? 'has-error' : ''}}">
    <label for="vision" class="control-label">{{ 'Vision' }}</label>
    <textarea class="form-control" rows="5" name="vision" type="textarea" id="vision" >{{ $setting->vision }}</textarea>
    {!! $errors->first('vision', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text_courtesy') ? 'has-error' : ''}}">
    <label for="text_courtesy" class="control-label">{{ 'Text Courtesy' }}</label>
    <textarea class="form-control" rows="5" name="text_courtesy" type="textarea" id="text_courtesy" >{{ $setting->text_courtesy }}</textarea>
    {!! $errors->first('text_courtesy', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('text_mail') ? 'has-error' : ''}}">
    <label for="text_mail" class="control-label">{{ 'Text Mail' }}</label>
    <textarea class="form-control" rows="5" name="text_mail" type="textarea" id="text_mail" >{{ $setting->text_mail }}</textarea>
    {!! $errors->first('text_mail', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
    <label for="latitude" class="control-label">{{ 'Latitude' }}</label>
    <input class="form-control" name="latitude" type="text" id="latitude" value="{{ $setting->latitude }}" >
    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
    <label for="longitude" class="control-label">{{ 'Longitude' }}</label>
    <input class="form-control" name="longitude" type="text" id="longitude" value="{{ $setting->longitude }}" >
    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar' : 'Create' }}">
</div>
