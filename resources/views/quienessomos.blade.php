@include('site.header')  
  <!-- Wrapper Open -->
  <div id="single-page" class="wrapper page-content single-page">
    <!-- ep-header Open -->
    <div class="ep-header">
      <style>
        .ep-header{background-image: url('theme/img/bg.jpg');}
      </style>
      <div class="container align-center">
        <strong class="sub-title semi-bold">Lorem Ipsum dolor sit amet</strong>
      </div>
    </div>
    <!-- ep-header Open --> 
    <!-- ep-body Open -->
    <div id="ep-body">
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-center bg-single"> 
            <div class="post row">
              <div class="col-md-7 col-sm-7 col-xs-12">
                <h1 class="post-title title-section bold"><span>Quienes somos</span></h1>
                <div class="entry">
                  <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
                  
                  <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
                  
                  <p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"</p>
                  
                  <h1>T&iacute;tulo uno</h1>
                  <h2>T&iacute;tulo uno</h2>
                  <h3>T&iacute;tulo uno</h3>
                  <h4>T&iacute;tulo uno</h4>
                  <h5>T&iacute;tulo uno</h5>
                  <h6>T&iacute;tulo uno</h6>                        
                </div>
              </div>
              <div id="ep-sidebar" class="col-md-5 col-sm-5 col-xs-12">
                @include('site.sidebar')               
                @include('site.social-media')                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- ep-body Open -->       
    @include('site.sub-footer') 
  </div>
  <!-- Wrapper Open -->
@include('site.footer')