<!-- Video Open -->
<div class="video-widget wdgt">
  <div class="row widget-header">
    <div class="col-md-12">
      <h2 class="title-section bold"><span>Video</span></h2>
    </div>
  </div>
  <div class="row widget-body">
    <div class="col-md-12">
      <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-ite" width="560" height="315" src="https://www.youtube.com/embed/beJ_yIL1_kI" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>
<!--Video Close-->