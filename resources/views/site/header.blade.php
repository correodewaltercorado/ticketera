<!DOCTYPE html>
<html lang="es-ES">
  <head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo asset('theme/assets/ico/favicon.ico')?>">
    
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo asset('theme/assets/ico/apple-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo asset('theme/assets/ico/apple-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo asset('theme/assets/ico/apple-icon-120x120.png')?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo asset('theme/assets/ico/android-icon-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo asset('theme/assets/ico/favicon-32x32.png')?>">
    <title>Smart Ticket</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex,follow" />
    <link rel="stylesheet" id="bootstrap-style-css"  href="<?php echo asset('theme/assets/css/bootstrap.css?ver=3.3.4')?>" type="text/css" media="all" />
    <script type="text/javascript" src="<?php echo asset('theme/assets/js/jquery.js?ver=1.11.1')?>"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo asset('theme/assets/js/html5shiv.js')?>"></script>
      <script src="<?php echo asset('theme/assets/js/respond.min.js')?>"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?php echo asset('theme/assets/css/style.css?ver=1.1')?>">
  </head>
  <body>
    <!-- Nav Open -->
    <nav id="navigation-bar" class="navbar navbar-default navbar-fixed-top">
      <!-- container Open -->
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed transition-ease" data-toggle="collapse" data-target="#NavCollapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <h1 id="site-title" class="navbar-brand">
            <a href="./">
              <img id="logo" class="animated" alt="1UP" src="<?php echo asset('theme/img/logoUp.png')?>" />
            </a>
          </h1>
                          
        </div>
        <!-- navbar-collapse Open -->
        <div class="collapse navbar-collapse" id="NavCollapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="current"><a href="{{ url('/') }}">Inicio</a></li>
            <li><a href="{{ url('/quienes-somos') }}">Quienes Somos</a></li>            
            <li><a href="{{ url('/eventos') }}">Eventos</a></li>
            <li><a href="#ep-contact">Contacto</a></li>            
          </ul>
        </div>
        <!-- navbar-collapse Close -->
      </div>
      <!-- container Close -->
    </nav>    
    <!-- Nav Close -->