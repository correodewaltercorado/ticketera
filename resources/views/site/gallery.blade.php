<!-- Gallery Open -->
<div class="gallery-widget wdgt">
  <div class="row widget-header">
    <div class="col-md-12">
      <h2 class="title-section bold"><span>Galer&iacute;as</span></h2>
    </div>
  </div>
  <div class="row widget-body">
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_1.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_1.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div>          
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_2.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_2.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div>    
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_3.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_3.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div> 
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_4.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_4.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div>      
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_5.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_5.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div>     
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->
    <!-- Gallery Item Open -->
    <div class="col-md-6 col-sm-6 col-xs-6 item">
      <div class="gallery-content">
        <a title="Nombre de la galer&iacute;a" href="theme/img/gallery/gallery_6.jpg" rel="gallery" class="light-box">
          <img alt="..." class="img-responsive" src="theme/img/gallery/320_6.jpg" />
          <div class="bgGallery transition-ease"><strong class="post-title">Nombre del galler&iacute;a</strong></div>
        </a>
      </div>
    </div>
    <!-- Gallery Item Close -->                       
  </div>
</div>
<!-- Gallery Close -->