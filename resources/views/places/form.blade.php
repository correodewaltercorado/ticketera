<div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
    <label for="country_id" class="control-label">{{ 'Pais' }}</label>
   

    <div class="col-md-12">
       @if (isset($place))
            <select name="country_id" class="form-control select2">
                @foreach($countries as $item)
                    <option value="{{ $item->id }}" @if($item->id == $place->country_id) selected @endif>{{ $item->name }}</option>
                @endforeach
                </select>
        @else
            <select name="country_id" class="form-control select2">
                @foreach($countries as $item)
                    <option value="{{ $item->id }}" >{{ $item->name }}</option>
                @endforeach
                </select>
        @endif
        {!! $errors->first('id_section', '<p class="help-block">:message</p>') !!}
    </div>

    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Nombre' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($place) ? $place->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Descripción' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($place) ? $place->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    <label for="address" class="control-label">{{ 'Dirección' }}</label>
    <input class="form-control" name="address" type="text" id="address" value="{{ isset($place) ? $place->address : ''}}" >
    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
    <label for="latitude" class="control-label">{{ 'Latitud' }}</label>
    <input class="form-control" name="latitude" type="text" id="latitude" value="{{ isset($place) ? $place->latitude : ''}}" >
    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
    <label for="longitude" class="control-label">{{ 'Longitud' }}</label>
    <input class="form-control" name="longitude" type="text" id="longitude" value="{{ isset($place) ? $place->longitude : ''}}" >
    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">{{ 'Imagen' }}</label>
    <textarea class="form-control" rows="5" name="image" type="textarea" id="image" >{{ isset($place) ? $place->image : ''}}</textarea>
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('map') ? 'has-error' : ''}}">
    <label for="map" class="control-label">{{ 'Mapa' }}</label>
    <textarea class="form-control" rows="5" name="map" type="textarea" id="map" >{{ isset($place) ? $place->map : ''}}</textarea>
    {!! $errors->first('map', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    <label for="active" class="control-label">{{ 'Activo' }}</label>
    <div class="radio">
    <label><input name="active" type="radio" value="1" {{ (isset($place) && 1 == $place->active) ? 'checked' : '' }}> Si</label>
</div>
<div class="radio">
    <label><input name="active" type="radio" value="0" @if (isset($place)) {{ (0 == $place->active) ? 'checked' : '' }} @else {{ 'checked' }} @endif> No</label>
</div>
    {!! $errors->first('active', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Actualizar   ' : 'Crear' }}">
</div>
