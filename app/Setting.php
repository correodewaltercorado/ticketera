<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'nit', 'comercial_name', 'business_name', 'mail', 'contact_text', 'address', 'telephone', 'facebook', 'instagram', 'twitter', 'youtube', 'about_as', 'mission', 'vision', 'text_courtesy', 'text_mail', 'latitude', 'longitude'];

    
}
