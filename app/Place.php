<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'places';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['country_id', 'name', 'description', 'address', 'latitude', 'longitude', 'image', 'map', 'active','created_by','updated_by'];

    public function country()
    {
        return $this->hasOne('App\Country','id','country_id');
    }

    
}
