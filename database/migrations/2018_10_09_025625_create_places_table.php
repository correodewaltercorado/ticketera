<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('country_id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->text('image')->nullable();
            $table->text('map')->nullable();
            $table->boolean('active')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            //Defino Relacion
            $table->foreign('country_id')
                ->references('id')
                ->on('countries');

            //Defino Relacion
            $table->foreign('created_by')
                ->references('id')
                ->on('users');

            //Defino Relacion
            $table->foreign('updated_by')
                ->references('id')
                ->on('users');


            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('places');
    }
}
