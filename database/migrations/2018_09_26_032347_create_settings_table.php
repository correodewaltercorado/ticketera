<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name',150)->nullable();
            $table->string('nit',150)->nullable();
            $table->string('comercial_name',150)->nullable();
            $table->string('business_name',150)->nullable();
            $table->string('mail',150)->nullable();
            $table->text('contact_text')->nullable();
            $table->string('address',150)->nullable();
            $table->string('telephone',150)->nullable();
            $table->text('facebook')->nullable();
            $table->text('instagram')->nullable();
            $table->text('twitter')->nullable();
            $table->text('youtube')->nullable();
            $table->text('about_as')->nullable();
            $table->text('mission')->nullable();
            $table->text('vision')->nullable();
            $table->text('text_courtesy')->nullable();
            $table->text('text_mail')->nullable();
            $table->string('latitude',150)->nullable();
            $table->string('longitude',150)->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('settings');
    }
}
